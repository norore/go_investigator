#! /usr/bin/env python3

import urllib.request
import json
import pprint
import sys

pp = pprint.PrettyPrinter(indent=4)

# GO:0019319
# hexose biosynthetic process

#goid = "0019319"
goid = sys.argv[1]

try:
    with urllib.request.urlopen("https://www.ebi.ac.uk/QuickGO/services/ontology/go/terms/GO:"+goid+"/descendants") as response:
        term = json.loads(response.read())
except:
    print("Erreur : le GO ID du terme est manquant ou n’est pas trouvé dans la base de données")
    exit()

pp.pprint(term)
